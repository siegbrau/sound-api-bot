package com.artamm.soundapi.shared.constants;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ConstText {
    public static final String MODIFIER_TEXT =
            "1. /speedup -  increase you audio speed \n" +
            "2. /cut - cut sound";

    public static final String CONVERTER_TEXT =
            "1. /towav -  convert to wav \n" +
                    "2. /tospct - convert to spectrogram";

//    TODO разобраться чего короткий адрес не работает
    public static final String FILEPATH_PREFIX ="C:/Users/Abysswalker/Desktop/projects/sound-api/src/main/resources/temp/";

    public static final String AUDIO_RETRIEVE_ERROR =" Unable to retrieve audio. See: ";
    public static final String AUDIO_SAVE_ERROR =" Unable to save audio. See:csdsd ";
    public static final String DELETE_FILE_ERROR = "Unable to delete file";


}
