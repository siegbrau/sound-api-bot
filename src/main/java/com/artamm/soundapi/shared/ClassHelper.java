package com.artamm.soundapi.shared;

import com.artamm.soundapi.api.model.Sound;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;

import static com.artamm.soundapi.shared.constants.ConstText.DELETE_FILE_ERROR;
import static com.artamm.soundapi.shared.constants.ConstText.FILEPATH_PREFIX;

public class ClassHelper {
	private static final Logger log = LogManager.getLogger(ClassHelper.class);

	public ClassHelper() {
		BasicConfigurator.configure();
	}

	public void saveFile(File convertedFile, AudioInputStream audioInputStream,
						 AudioFormat convertFormat) throws IOException {
		AudioInputStream convertedAudio =
				AudioSystem.getAudioInputStream(convertFormat, audioInputStream);
		AudioSystem.write(convertedAudio,
				AudioFileFormat.Type.WAVE, convertedFile);
	}

	public void deleteFile(Sound sound) {
		try {
			Files.delete(Path.of(sound.getFilepath()));
		} catch (IOException e) {
			log.info(DELETE_FILE_ERROR + e.getCause());
		}
	}

	//    requiredMimeType examples: .wav, .ogg, .mp3
	public Sound convertedTemplate(String filename, String requiredMimeType) {
		Sound converted = new Sound();
		converted.setFilename(filename.split("\\.")[0] + requiredMimeType);
		converted.setMimeType(requiredMimeType);
		converted.setFilepath(FILEPATH_PREFIX + "converted/" + converted.getFilename());
		return converted;
	}

	public void UploadFiles(String fileName,
							byte[] bFile) throws IOException, UnsupportedAudioFileException {
		String uploadedFileLocation = FILEPATH_PREFIX + "converted/";

		AudioInputStream source;
		AudioInputStream pcm;
		InputStream b_in = new ByteArrayInputStream(bFile);
		source = AudioSystem.getAudioInputStream(new BufferedInputStream(b_in));
		pcm = AudioSystem.getAudioInputStream(AudioFormat.Encoding.PCM_SIGNED, source);
		File newFile = new File(uploadedFileLocation + fileName);
		AudioSystem.write(pcm, AudioFileFormat.Type.WAVE, newFile);

		source.close();
		pcm.close();
	}

}
