package com.artamm.soundapi;

import com.artamm.soundapi.telegram.bot.Bot;
import com.artamm.soundapi.telegram.bot.BotWebHook;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.api.methods.updates.SetWebhook;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;

@SpringBootApplication
public class SoundApiApplication {
    private static final Logger log = LogManager.getLogger(SoundApiApplication.class);

    public static void main(String[] args) {
        BasicConfigurator.configure();
        try {
            TelegramBotsApi telegramBotsApi = new TelegramBotsApi(DefaultBotSession.class);
            telegramBotsApi.registerBot(new Bot());

//            telegramBotsApi.registerBot(new BotWebHook(), SetWebhook.builder().build());
        } catch (TelegramApiException e) {
            log.info(e.getMessage());
        }
        SpringApplication.run(SoundApiApplication.class, args);
    }

}
