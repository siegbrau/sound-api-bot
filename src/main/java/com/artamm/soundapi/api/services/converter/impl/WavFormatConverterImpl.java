package com.artamm.soundapi.api.services.converter.impl;

import com.artamm.soundapi.api.model.Sound;
import com.artamm.soundapi.api.model.Spectrogram;
import com.artamm.soundapi.api.services.converter.FormatConverter;
import com.artamm.soundapi.shared.ClassHelper;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class WavFormatConverterImpl implements FormatConverter {
	private static final Logger LOGGER = LogManager.getLogger(WavFormatConverterImpl.class);
	private static final ClassHelper classHelper = new ClassHelper();

	@Override
	public Sound convertToWAV(Sound sound) {
		return sound;
	}

	@Override
	public Sound convertToMP3(Sound sound) {
		//prepare sound for converse
		Sound convertedSound = classHelper.convertedTemplate(sound.getFilename(), ".mp3");
		File convertedFile = new File(convertedSound.getFilepath());
		try {
			InputStream stream = new FileInputStream(new File(sound.getFilepath()));
			classHelper.UploadFiles(convertedSound.getFilename(), stream.readAllBytes());
			convertedSound.setFile(new FileInputStream(convertedFile));
		} catch (UnsupportedAudioFileException e) {
			LOGGER.info(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return convertedSound;
	}

	@Override
	public Sound convertToOGG(Sound sound) {
		//prepare sound for converse
		Sound convertedSound = classHelper.convertedTemplate(sound.getFilename(), ".ogg");
		File convertedFile = new File(convertedSound.getFilepath());

		try {
			InputStream stream = new FileInputStream(new File(sound.getFilepath()));

			//            https://stackoverflow.com/questions/41784397/convert-mp3-to-wav-in-java
			AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(stream);
			AudioFormat format = audioInputStream.getFormat();
			AudioFormat convertFormat = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED,
					format.getSampleRate(), 16,
					format.getChannels(),
					format.getChannels() * 2,
					format.getSampleRate(),
					false);

			classHelper.saveFile(convertedFile, audioInputStream, convertFormat);
			convertedSound.setFile(new FileInputStream(convertedFile));
		} catch (UnsupportedAudioFileException e) {
			LOGGER.info(e.getMessage());
		} catch (IOException e) {
			LOGGER.info(e.getMessage() + e.getCause());
		}

		//        Remove file from directory
		//        classHelper.deleteFile(sound);
		return convertedSound;

	}

	@Override
	public Spectrogram convertToSpectrogram(Sound sound) {
		return null;
	}

	@Override
	public Sound convertToSound(Spectrogram spectrogram) {
		return null;
	}
}
