package com.artamm.soundapi.api.services.modifier.impl;

import com.artamm.soundapi.api.model.Sound;
import com.artamm.soundapi.api.services.modifier.SoundModifier;

public class SoundModifierImpl implements SoundModifier {
    @Override
    public Sound speedUp(Sound sound, double speed) {
        return null;
    }

    @Override
    public Sound slowDown(Sound sound, double speed) {
        return null;
    }

    @Override
    public Sound cutSound(Sound sound, double start, double end) {
        return null;
    }

    @Override
    public Sound tuneOn(Sound sound, double rate) {
        return null;
    }

    @Override
    public Sound tuneDown(Sound sound, double rate) {
        return null;
    }

    @Override
    public Sound playback(Sound sound) {
        return null;
    }
}
