package com.artamm.soundapi.api.services.modifier;

import com.artamm.soundapi.api.model.Sound;

public interface SoundModifier {
    Sound speedUp(Sound sound, double speed);
    Sound slowDown(Sound sound, double speed);
    Sound cutSound(Sound sound, double start, double end);
    Sound tuneOn(Sound sound, double rate);
    Sound tuneDown(Sound sound, double rate);
    Sound playback(Sound sound);

}
