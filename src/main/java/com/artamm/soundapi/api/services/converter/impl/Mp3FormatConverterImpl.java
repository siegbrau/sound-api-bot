package com.artamm.soundapi.api.services.converter.impl;

import com.artamm.soundapi.shared.ClassHelper;
import com.artamm.soundapi.api.services.converter.FormatConverter;
import com.artamm.soundapi.api.model.Sound;
import com.artamm.soundapi.api.model.Spectrogram;
import lombok.extern.log4j.Log4j;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

@Component
@Log4j
public class Mp3FormatConverterImpl implements FormatConverter {
    private static final Logger LOGGER = LogManager.getLogger(Mp3FormatConverterImpl.class);
    private static final ClassHelper classHelper = new ClassHelper();

    public Mp3FormatConverterImpl() {
        BasicConfigurator.configure();
    }

    @Override
    public Sound convertToWAV(Sound sound) {
        //prepare sound for converse
        Sound convertedSound = classHelper.convertedTemplate(sound.getFilename(),".wav");
        File convertedFile = new File(convertedSound.getFilepath());

        try {
            InputStream stream = new FileInputStream(new File(sound.getFilepath()));

            //            https://stackoverflow.com/questions/41784397/convert-mp3-to-wav-in-java
            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(stream);
            AudioFormat format = audioInputStream.getFormat();
            AudioFormat convertFormat = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED,
                    format.getSampleRate(), 16,
                    format.getChannels(),
                    format.getChannels() * 2,
                    format.getSampleRate(),
                    false);

            classHelper.saveFile(convertedFile, audioInputStream, convertFormat);
            convertedSound.setFile(new FileInputStream(convertedFile));
        } catch (UnsupportedAudioFileException e) {
            LOGGER.info(e.getMessage());
        } catch (IOException e) {
            LOGGER.info(e.getMessage() + e.getCause());
        }

        //        Remove file from directory
//        classHelper.deleteFile(sound);
        return convertedSound;
    }

    @Override
    public Sound convertToMP3(Sound sound) {
        return sound;
    }

    @Override
    public Sound convertToOGG(Sound sound) {
        //prepare sound for converse
        Sound convertedSound = classHelper.convertedTemplate(sound.getFilename(),".flac");
        File convertedFile = new File(convertedSound.getFilepath());

        try {
            InputStream stream = new FileInputStream(new File(sound.getFilepath()));

            //            https://stackoverflow.com/questions/41784397/convert-mp3-to-wav-in-java
            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(stream);
            AudioFormat format = audioInputStream.getFormat();
            AudioFormat convertFormat = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED,
                    format.getSampleRate(), 16,
                    format.getChannels(),
                    format.getChannels() * 2,
                    format.getSampleRate(),
                    false);

            classHelper.saveFile(convertedFile, audioInputStream, convertFormat);
            convertedSound.setFile(new FileInputStream(convertedFile));
        } catch (UnsupportedAudioFileException e) {
            LOGGER.info(e.getMessage());
        } catch (IOException e) {
            LOGGER.info(e.getMessage() + e.getCause());
        }

        //        Remove file from directory
        //        classHelper.deleteFile(sound);
        return convertedSound;
    }

    @Override
    public Spectrogram convertToSpectrogram(Sound sound) {
        return null;
    }

    private void saveFile(File convertedFile, AudioInputStream audioInputStream,
                          AudioFormat convertFormat) throws IOException {
        AudioInputStream convertedAudio =
                AudioSystem.getAudioInputStream(convertFormat, audioInputStream);
        AudioSystem.write(convertedAudio,
                AudioFileFormat.Type.WAVE, convertedFile);
    }



    @Override
    public Sound convertToSound(Spectrogram spectrogram) {
        return null;
    }

}
