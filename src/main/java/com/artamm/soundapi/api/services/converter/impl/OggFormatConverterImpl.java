package com.artamm.soundapi.api.services.converter.impl;

import com.artamm.soundapi.api.model.Sound;
import com.artamm.soundapi.api.model.Spectrogram;
import com.artamm.soundapi.shared.ClassHelper;
import com.artamm.soundapi.api.services.converter.FormatConverter;

import lombok.extern.log4j.Log4j;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import static com.artamm.soundapi.shared.constants.ConstText.FILEPATH_PREFIX;

@Log4j
public class OggFormatConverterImpl implements FormatConverter {
	private static final Logger LOGGER = LogManager.getLogger(OggFormatConverterImpl.class);
	private static final ClassHelper classHelper = new ClassHelper();

	public OggFormatConverterImpl() {
		BasicConfigurator.configure();
	}

	@Override
	public Sound convertToWAV(Sound sound) {

		//prepare sound for converse
		Sound convertedSound = classHelper.convertedTemplate(sound.getFilename(), ".wav");
		File convertedFile = new File(convertedSound.getFilepath());

		try {
			File sourceFile = new File(sound.getFilepath());

			AudioInputStream in = AudioSystem.getAudioInputStream(sourceFile);
			AudioInputStream din = null;
			AudioFormat baseFormat = in.getFormat();

			AudioFormat decodedFormat = new AudioFormat(
					AudioFormat.Encoding.PCM_SIGNED, baseFormat.getSampleRate(),
					16, baseFormat.getChannels(), baseFormat.getChannels() * 2,
					baseFormat.getSampleRate(), false);

			din = AudioSystem.getAudioInputStream(decodedFormat, in);
			AudioSystem.write(din, AudioFileFormat.Type.WAVE, convertedFile);
			convertedSound.setFile(new FileInputStream(convertedFile));
		} catch (UnsupportedAudioFileException e) {
			LOGGER.info(e.getMessage());
		} catch (IOException e) {
			LOGGER.info(e.getMessage() + e.getCause());
		}

		//        Remove file from directory
		//        classHelper.deleteFile(sound);
		return convertedSound;
	}

	@Override
	public Sound convertToMP3(Sound sound) {
		//prepare sound for converse
		Sound convertedSound = classHelper.convertedTemplate(sound.getFilename(), ".mp3");
		File convertedFile = new File(convertedSound.getFilepath());
		try {
			InputStream stream = new FileInputStream(new File(sound.getFilepath()));
			classHelper.UploadFiles(convertedSound.getFilename(), stream.readAllBytes());
			convertedSound.setFile(new FileInputStream(convertedFile));
		} catch (UnsupportedAudioFileException e) {
			LOGGER.info(e.getMessage());
		} catch (IOException e) {
			LOGGER.error("Unable to convert to MP3. See:", e);
		}
		return convertedSound;

	}

	@Override
	public Sound convertToOGG(Sound sound) {
		//prepare sound for converse
		Sound convertedSound = classHelper.convertedTemplate(sound.getFilename(), ".ogg");
		File convertedFile = new File(convertedSound.getFilepath());

		try {
			InputStream stream = new FileInputStream(new File(sound.getFilepath()));
			classHelper.UploadFiles(convertedSound.getFilename(), stream.readAllBytes());
			convertedSound.setFile(new FileInputStream(convertedFile));
		} catch (UnsupportedAudioFileException e) {
			LOGGER.info(e.getMessage());
		} catch (IOException e) {
			LOGGER.info(e.getMessage() + e.getCause());
		}

		//        Remove file from directory
		//        classHelper.deleteFile(sound);
		return convertedSound;

	}

	@Override
	public Spectrogram convertToSpectrogram(Sound sound) {
		return null;
	}

	@Override
	public Sound convertToSound(Spectrogram spectrogram) {
		return null;
	}
}
