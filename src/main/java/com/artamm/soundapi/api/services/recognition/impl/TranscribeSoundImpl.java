package com.artamm.soundapi.api.services.recognition.impl;

import com.artamm.soundapi.api.services.converter.FormatConverter;
import com.artamm.soundapi.api.services.converter.impl.Mp3FormatConverterImpl;
import com.artamm.soundapi.api.model.Sound;
import com.artamm.soundapi.api.services.recognition.TranscribeSound;
import edu.cmu.sphinx.api.Configuration;
import edu.cmu.sphinx.api.SpeechResult;
import edu.cmu.sphinx.api.StreamSpeechRecognizer;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

@Component
@Slf4j
public class TranscribeSoundImpl implements TranscribeSound {
    private static final Logger LOGGER = LoggerFactory.getLogger(TranscribeSoundImpl.class);
    private static final String ACOUSTIC_MODEL_PATH =
            "src/main/resources/lang/sphinx-ru/zero_ru.cd_cont_4000";
    private static final String DICTIONARY_PATH =
            "src/main/resources/lang/sphinx-ru/ru.dic";
    private static final String LANGUAGE_MODEL =
            "src/main/resources/lang/sphinx-ru/ru.lm";

    private Configuration configuration = new Configuration();

    public TranscribeSoundImpl() {
        configuration.setAcousticModelPath(ACOUSTIC_MODEL_PATH);
        configuration.setDictionaryPath(DICTIONARY_PATH);
        configuration.setLanguageModelPath(LANGUAGE_MODEL);
        configuration.setSampleRate(8000);
    }

    @Override
    public String recognize(Sound input) {
        String audioToText = "";
        Sound sound;
        //        Prepare audio as wav
        if (!input.getMimeType().contains("wav")) {
            FormatConverter formatConverter = new Mp3FormatConverterImpl();
            sound = formatConverter.convertToWAV(input);
        } else {
            sound = input;
        }
        StreamSpeechRecognizer recognizer = setStreamSpeechRecognizer();
        recognizer.startRecognition(sound.getFile());
        SpeechResult result;
        while ((result = recognizer.getResult()) != null) {
            try {
                audioToText = result.getHypothesis();
            } catch (Exception e) {
                LOGGER.warn("Unable to transcribe. Reason: {0}", e.getCause());
            }
        }

        return audioToText;
    }

    @Override
    public String recognize(File file) throws FileNotFoundException {
        String audioToText = "";
        InputStream inputStream = new FileInputStream(file);

        StreamSpeechRecognizer recognizer = setStreamSpeechRecognizer();
        recognizer.startRecognition(inputStream);
        SpeechResult result;
        while ((result = recognizer.getResult()) != null) {
            try {
                audioToText = result.getHypothesis();
            } catch (Exception e) {
                LOGGER.warn("Unable to transcribe. Reason: {0}", e.getCause());
            }
        }

        return audioToText;
    }

    private StreamSpeechRecognizer setStreamSpeechRecognizer() {
        try {
            return new StreamSpeechRecognizer(
                    configuration);
        } catch (IOException e) {
            LOGGER.warn("Unable to set recongizer. Reason: {0}", e.getCause());
        }
        throw new AssertionError("Unable to set speech recognizer");
    }
}
