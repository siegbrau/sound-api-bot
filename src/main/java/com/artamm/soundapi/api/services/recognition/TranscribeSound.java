package com.artamm.soundapi.api.services.recognition;

import com.artamm.soundapi.api.model.Sound;

import java.io.File;
import java.io.FileNotFoundException;

public interface TranscribeSound {

     String recognize(Sound sound);
     String recognize(File file) throws FileNotFoundException;

}
