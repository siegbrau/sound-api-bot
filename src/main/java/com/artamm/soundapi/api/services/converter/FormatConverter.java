package com.artamm.soundapi.api.services.converter;

import com.artamm.soundapi.api.model.Sound;
import com.artamm.soundapi.api.model.Spectrogram;

public interface FormatConverter {
    Sound convertToWAV(Sound sound);
    Sound convertToMP3(Sound sound);
    Sound convertToOGG(Sound Sound);
    Spectrogram convertToSpectrogram(Sound sound);
    Sound convertToSound(Spectrogram spectrogram);
}
