package com.artamm.soundapi.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.tika.Tika;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.telegram.telegrambots.meta.api.objects.Audio;
import org.telegram.telegrambots.meta.api.objects.Document;
import org.telegram.telegrambots.meta.api.objects.Voice;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import static com.artamm.soundapi.shared.constants.ConstText.FILEPATH_PREFIX;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Sound {
	private static final Logger LOGGER = LoggerFactory.getLogger(Sound.class);

	private String id;
	private InputStream file;
	private String mimeType;
	private String filename;
	private String filepath;

	public Sound(InputStream file) {
		this.file = file;
	}

	public Sound(File file) {
		try {
			this.file = new FileInputStream(file);
			this.mimeType = new Tika().detect(file);
			this.filename = file.getName();
			this.filepath = file.getPath();

		} catch (IOException e) {
			String message = String.format("Unable to create Sound. %n Error:%s %n Caused: %s",
					e.getMessage(), e.getCause());
			LOGGER.error(message);
		}
	}

	public Sound(Document audio, InputStream inputStream) {
		this.file = inputStream;
		this.mimeType = audio.getMimeType();
		this.filename = audio.getFileName();
		this.filepath = FILEPATH_PREFIX + audio.getFileName();
	}

	public Sound(Audio audio, InputStream inputStream) {
		this.file = inputStream;
		this.mimeType = audio.getMimeType();
		this.filename = audio.getFileName();
		this.filepath = FILEPATH_PREFIX + audio.getFileName();
	}

	public Sound(Voice audio, InputStream inputStream) {
		this.file = inputStream;
		this.mimeType = audio.getMimeType();
		this.filename = "voiceMessage" + audio.getFileId() + ".ogg";
		this.filepath = FILEPATH_PREFIX + filename;
	}
}

