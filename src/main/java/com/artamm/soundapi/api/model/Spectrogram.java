package com.artamm.soundapi.api.model;

import lombok.Data;

@Data
public class Spectrogram extends Sound {
    private double height;
    private double width;
}
