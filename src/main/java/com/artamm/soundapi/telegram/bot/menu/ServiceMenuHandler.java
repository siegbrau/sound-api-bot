package com.artamm.soundapi.telegram.bot.menu;

import com.artamm.soundapi.api.model.Sound;

public interface ServiceMenuHandler {
    Sound handleMessage(String command, Sound sound);
}
