package com.artamm.soundapi.telegram.bot.menu;

import com.artamm.soundapi.api.model.Sound;
import com.artamm.soundapi.api.services.recognition.TranscribeSound;
import com.artamm.soundapi.api.services.recognition.impl.TranscribeSoundImpl;
import com.artamm.soundapi.telegram.bot.menu.impl.ConverterMenuHandler;
import com.artamm.soundapi.telegram.bot.menu.impl.ModifierMenuHandler;
import lombok.NoArgsConstructor;
import org.telegram.telegrambots.meta.api.methods.send.SendAudio;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.artamm.soundapi.shared.constants.ConstText.CONVERTER_TEXT;
import static com.artamm.soundapi.shared.constants.ConstText.MODIFIER_TEXT;

@NoArgsConstructor
public class MainMenuHandler {
	private Map<String, Enum<Param>> commands = new HashMap<>(Map.of(
			"speedup", Param.MODIFIER,
			"slowdown", Param.MODIFIER,
			"tuneon", Param.MODIFIER,
			"tunedown", Param.MODIFIER,
			"playback", Param.MODIFIER,
			"towav", Param.CONVERTER,
			"tomp3", Param.CONVERTER,
			"toogg", Param.CONVERTER,
			"tospct", Param.CONVERTER,
			"rcg", Param.RECOGNIZE));

	public SendMessage helloMessage(String chatId) {
		return SendMessage.builder()
						  .chatId(chatId)
						  .text("This bot can convert & modify your audio. Press buttons for more information")
						  .replyMarkup(MainMenuHandler::helpKeyboard)
						  .build();
	}

	private static ReplyKeyboardMarkup helpKeyboard() {
		List<KeyboardRow> keyboard = new ArrayList<>();
		KeyboardRow row = new KeyboardRow();
		row.add("Help");
		keyboard.add(row);
		return ReplyKeyboardMarkup.builder()
								  .keyboard(keyboard)
								  .resizeKeyboard(true)
								  .build();
	}

	//    Creates help message keyboard wih Query Callbacks for Modifier function
	public SendMessage createModifierHelpBoardMessage(String chatId) {

		//        Create row and adds buttons in t
		List<InlineKeyboardButton> keyboardButtonsRow1 = new ArrayList<>();
		keyboardButtonsRow1.add(InlineKeyboardButton.builder()
													.text("/speedup")
													.callbackData("/speedup")
													.build());

		keyboardButtonsRow1.add(InlineKeyboardButton.builder()
													.text("/slowdown")
													.callbackData("/slowdown")
													.build());

		keyboardButtonsRow1.add(InlineKeyboardButton.builder()
													.text("/cutdown")
													.callbackData("/cutdown")
													.build());

		return getSendMessage(chatId, keyboardButtonsRow1, MODIFIER_TEXT);
	}

	//    Creates help message keyboard wih Query Callbacks for Converter function
	public SendMessage createConverterHelpBoardMessage(String chatId) {

		//        Create row and adds buttons in t
		List<InlineKeyboardButton> keyboardButtonsRow1 = new ArrayList<>();
		keyboardButtonsRow1.add(InlineKeyboardButton.builder()
													.text("/towav")
													.callbackData("/towav")
													.build());

		keyboardButtonsRow1.add(InlineKeyboardButton.builder()
													.text("/tomp3")
													.callbackData("/tomp3")
													.build());

		keyboardButtonsRow1.add(InlineKeyboardButton.builder()
													.text("/tospct")
													.callbackData("/tospct")
													.build());

		return getSendMessage(chatId, keyboardButtonsRow1, CONVERTER_TEXT);
	}

	//    Creates message with keyboard
	private SendMessage getSendMessage(String chatId,
									   List<InlineKeyboardButton> keyboardButtonsRow1,
									   String converterText) {
		List<List<InlineKeyboardButton>> rowList = new ArrayList<>();
		rowList.add(keyboardButtonsRow1);

		InlineKeyboardMarkup keyBoardMarkup = new InlineKeyboardMarkup();
		keyBoardMarkup.setKeyboard(rowList);

		return SendMessage.builder()
						  .chatId(chatId)
						  .text(converterText)
						  .replyMarkup(keyBoardMarkup)
						  .build();
	}

	public SendMessage sendCommandMessage(String chatId) {
		return SendMessage.builder()
						  .text("Send command with parameter. Example: speedUp 2")
						  .chatId(chatId)
						  .build();
	}

	public String findCommand(String message) {
		if (commands.containsKey(message.split("\\s+")[0])) {
			return message;
		} else {
			return "";
		}
	}

	public SendAudio returnAudio(String currentCommand, Sound currentSound, String chatId) {
		Sound sound;
		if (commands.get(currentCommand.split("\\s+")[0]).name().equals(Param.CONVERTER.name())) {
			ServiceMenuHandler serviceMenuHandler =
					new ConverterMenuHandler(currentSound.getMimeType());
			sound = serviceMenuHandler.handleMessage(currentCommand, currentSound);
		} else {
			ServiceMenuHandler serviceMenuHandler = new ModifierMenuHandler();
			sound = serviceMenuHandler.handleMessage(currentCommand, currentSound);
		}

		InputFile inputFile = new InputFile(new File(sound.getFilepath()));
		return SendAudio.builder()
						.audio(inputFile)
						.chatId(chatId)
						.build();
	}

	public SendMessage transribeSound(Sound currentSound,
									  String chatId) throws FileNotFoundException {
		File file = new File(currentSound.getFilepath());
		TranscribeSound transcribeSound = new TranscribeSoundImpl();
		String text = transcribeSound.recognize(file);
		return SendMessage.builder()
						  .chatId(chatId)
						  .text(text)
						  .build();

	}
}
