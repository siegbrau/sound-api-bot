package com.artamm.soundapi.telegram.bot.menu.impl;

import com.artamm.soundapi.api.model.Sound;
import com.artamm.soundapi.api.services.converter.FormatConverter;
import com.artamm.soundapi.api.services.converter.impl.Mp3FormatConverterImpl;
import com.artamm.soundapi.api.services.converter.impl.OggFormatConverterImpl;
import com.artamm.soundapi.api.services.converter.impl.WavFormatConverterImpl;
import com.artamm.soundapi.telegram.bot.menu.ServiceMenuHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

@Slf4j
public class ConverterMenuHandler implements ServiceMenuHandler {
	private static final Logger LOGGER = LogManager.getLogger(ConverterMenuHandler.class);
	private FormatConverter formatConverter;

	public ConverterMenuHandler(String mimeType) {
		defineConverterType(mimeType);
	}

	@Override
	public Sound handleMessage(String command, Sound sound) {
		switch (command) {
			case "towav": {
				return formatConverter.convertToWAV(sound);
			}
			case "tomp3": {
				return formatConverter.convertToMP3(sound);
			}
			case "toogg": {
				return formatConverter.convertToOGG(sound);
			}
			default: {
				LOGGER.warn("Unable to convert from this format");
				return new Sound();
			}
		}
	}

	private void defineConverterType(String mimeType) {
		if (mimeType.contains("wav")) {
			formatConverter = new WavFormatConverterImpl();
		} else if (mimeType.contains("mpeg") || mimeType.contains(("mp3"))) {
			formatConverter = new Mp3FormatConverterImpl();
		} else if (mimeType.contains("ogg")) {
			formatConverter = new OggFormatConverterImpl();
		}
	}
}
