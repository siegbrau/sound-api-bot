package com.artamm.soundapi.telegram.bot;

import com.artamm.soundapi.api.model.Sound;

import com.artamm.soundapi.telegram.bot.menu.MainMenuHandler;
import com.google.common.io.Files;
import lombok.extern.log4j.Log4j;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.GetFile;
import org.telegram.telegrambots.meta.api.objects.Audio;
import org.telegram.telegrambots.meta.api.objects.Document;
import org.telegram.telegrambots.meta.api.objects.Update;

import org.telegram.telegrambots.meta.api.objects.Voice;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import static com.artamm.soundapi.shared.constants.ConstText.AUDIO_RETRIEVE_ERROR;
import static com.artamm.soundapi.shared.constants.ConstText.AUDIO_SAVE_ERROR;

@Component
@Log4j
public class Bot extends TelegramLongPollingBot {
	private static final Logger LOGGER = LogManager.getLogger(Bot.class);
	private static final MainMenuHandler MENU_HANDLER = new MainMenuHandler();
	private Sound currentSound;
	private String currentCommand = "";

	@Value("${telegram.token}")
	private String token;

	@Value("${telegram.username}")
	private String username;

	@Override
	public String getBotUsername() {
		return username;
	}

	@Override
	public String getBotToken() {
		return token;
	}

	@Override
	public void onUpdateReceived(Update update) {
		try {
			extractCaptionCommandIfExists(update);
			extractSoundIfExist(update);

			//            handle text input if it exists
			if (update.getMessage().hasText()) {
				//            says hello
				if (update.getMessage().getText().toLowerCase().contains("start")) {
					execute(MENU_HANDLER.helloMessage(update.getMessage().getChatId().toString()));
				}
				//            handles  help
				else if (update.getMessage().getText().toLowerCase().contains("help")) {
					execute(MENU_HANDLER.createConverterHelpBoardMessage(
							update.getMessage().getChatId().toString()));
					execute(MENU_HANDLER.createModifierHelpBoardMessage(
							update.getMessage().getChatId().toString()));
				}
				//             if neither it's command
				else {
					currentCommand =
							MENU_HANDLER.findCommand(update.getMessage().getText().toLowerCase());
				}
			}
			//            Execute command
			if (!currentCommand.equals("") && currentSound != null) {
				if (currentCommand.contains("rcg")) {
					execute(MENU_HANDLER.transribeSound(currentSound,
							update.getMessage().getChatId().toString()));
				} else {
					execute(MENU_HANDLER.returnAudio(currentCommand, currentSound,
							update.getMessage().getChatId().toString()));
				}
				currentCommand = "";
			}

		} catch (TelegramApiException | FileNotFoundException e) {
			LOGGER.info("Unable to send update. See:" + e.getCause());
		}
	}

	private void extractCaptionCommandIfExists(Update update) {
		if (update.getMessage().getCaption() != null) {
			currentCommand =
					MENU_HANDLER.findCommand(update.getMessage().getCaption().toLowerCase());
		}
	}

	private void sendMessageIfCommandIsNotSet(String chatId) throws TelegramApiException {
		if (currentCommand.equals("")) {
			execute(MENU_HANDLER.sendCommandMessage(chatId));
		}
	}

	private void extractSoundIfExist(Update update) throws TelegramApiException {
		// Handle input first
		if (update.getMessage().hasAudio()) {
			currentSound = extractAudio(update.getMessage().getAudio());
		}
		if (update.getMessage().hasVoice()) {
			currentSound = extractVoice(update.getMessage().getVoice());
		}
		if (update.getMessage().hasDocument()) {
			currentSound = extractDocument(update.getMessage().getDocument());
		}

		sendMessageIfCommandIsNotSet(update.getMessage().getChatId().toString());
	}

	private Sound extractVoice(Voice voice) {
		GetFile getFile = GetFile.builder()
								 .fileId(voice.getFileId())
								 .build();
		String filePath = getFilePath(getFile);

		try {
			Sound extracted = new Sound(voice, downloadFileAsStream(filePath));
			File upload = new File(extracted.getFilepath());
			Files.write(extracted.getFile().readAllBytes(), upload);
			return extracted;
		} catch (TelegramApiException | IOException e) {
			LOGGER.info(AUDIO_SAVE_ERROR + e.getMessage());
		}

		throw new AssertionError("Unable to extract Audio");
	}

	private Sound extractAudio(Audio audio) {
		GetFile getFile = GetFile.builder()
								 .fileId(audio.getFileId())
								 .build();

		String filePath = getFilePath(getFile);
		try {
			Sound extracted = new Sound(audio, downloadFileAsStream(filePath));
			File upload = new File(extracted.getFilepath());
			Files.write(extracted.getFile().readAllBytes(), upload);
			return extracted;
		} catch (TelegramApiException | IOException e) {
			LOGGER.info(AUDIO_SAVE_ERROR + e.getMessage());
		}

		throw new AssertionError("Unable to extract Voice");
	}

	//    If message is resent
	private Sound extractDocument(Document audio) {
		GetFile getFile = GetFile.builder()
								 .fileId(audio.getFileId())
								 .build();

		String filePath = getFilePath(getFile);
		try {
			Sound extracted = new Sound(audio, downloadFileAsStream(filePath));
			File upload = new File(extracted.getFilepath());
			Files.write(extracted.getFile().readAllBytes(), upload);
			return extracted;
		} catch (TelegramApiException | IOException e) {
			LOGGER.info(AUDIO_SAVE_ERROR + e.getMessage());
		}

		throw new AssertionError("Unable to extract Voice");
	}

	private String getFilePath(GetFile getFile) {
		try {
			return (execute(getFile).getFilePath());
		} catch (TelegramApiException e) {
			LOGGER.info(AUDIO_RETRIEVE_ERROR + e.getMessage());
		}
		throw new AssertionError("Unable to extract filepath");
	}

}
