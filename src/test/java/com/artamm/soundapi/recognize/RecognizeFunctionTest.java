package com.artamm.soundapi.recognize;

import com.artamm.soundapi.api.model.Sound;
import com.artamm.soundapi.api.services.recognition.TranscribeSound;
import com.artamm.soundapi.api.services.recognition.impl.TranscribeSoundImpl;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.io.File;

public class RecognizeFunctionTest {

    private static final String path = "src/test/resources/temp/converted/decoder-test.wav";

    @Test
    public void VerifyTextIsRecognized() {

        File mp3File = new File(path);
        Sound sound = new Sound(mp3File);
        TranscribeSound transcribeSound = new TranscribeSoundImpl();
        String result = transcribeSound.recognize(sound);
        Assert.assertNotNull(result);
        System.out.println(result);
    }
}
