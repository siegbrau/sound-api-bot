package com.artamm.soundapi.converter;

import com.artamm.soundapi.api.services.converter.FormatConverter;
import com.artamm.soundapi.api.services.converter.impl.Mp3FormatConverterImpl;
import com.artamm.soundapi.api.model.Sound;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.io.File;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.StringEndsWith.endsWith;

public class Mp3FormatConverterTest {

    private Sound sound;
    private FormatConverter formatConverter = new Mp3FormatConverterImpl();
    private static final String path = "src/test/resources/files/audio/domoj.mp3";

    @Test
    public void EnsureFileConvertedToWAV() {
        File mp3File = new File(path);
        sound = new Sound(mp3File);
        sound.setFilename("bubub");
        Sound converted = formatConverter.convertToWAV(sound);
        Assert.assertNotNull("Converted file IS NULL", converted.getFile());
        assertThat("Converted file doesn't have .wav format. Filename:" + converted.getFilename(),
                converted.getFilename(), endsWith(".wav"));
    }

    //    @Test
    //    public void EnsureFileConvertedToOGG() {
    //        File mp3File = new File(path);
    //        sound = new Sound(mp3File);
    //        sound.setFilename("domoj");
    //        Sound converted = formatConverter.convertToWAV(sound);
    //        Assert.assertNotNull(converted);
    //        Assert.assertTrue(sound.getFilename().contains(".wav"));
    //    }

    @Test
    public void EnsureConvertedFileIsInTempFolder() {
        Assert.assertNotNull(sound);
    }
}
